<?php declare(strict_types=1);

namespace Plugin\jtl_export_smarty_func;

use JTL\Catalog\Product\Artikel;
use JTL\Events\Dispatcher;
use JTL\Export\FormatExporter;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;

/**
 * Class Bootstrap
 * @package Plugin\jtl_export_smarty_func
 */
class Bootstrap extends Bootstrapper
{
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_EXPORT_START, function (array $args) {
            /** @var FormatExporter $exporter */
            $exporter = $args['exporter'];
            $smarty   = $exporter->getSmarty();
            $smarty->registerPlugin(\Smarty::PLUGIN_MODIFIER, 'reverseTest', [$this, 'smartyReverseTest']);
            $smarty->registerPlugin(\Smarty::PLUGIN_FUNCTION, 'randomIntTest', [$this, 'randomIntTest']);
        });
    }

    public function smartyReverseTest(string $string): string
    {
        return \strrev($string);
    }

    public function randomIntTest($args): int
    {
        $min = (int)($args['min'] ?? 0);
        $max = (int)($args['max'] ?? 999);

        return \random_int($min, $max);
    }
}
